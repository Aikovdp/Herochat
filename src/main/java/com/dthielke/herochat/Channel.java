/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat;

import com.dthielke.herochat.MessageNotFoundException;
import com.dthielke.herochat.ChannelChatEvent;
import com.dthielke.herochat.util.Messaging;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

public interface Channel extends TagFormatter {
    
    boolean addMember(@NotNull Chatter chatter, boolean announce, boolean flagUpdate);
    
    void addWorld(@NotNull String world);
    
    /**
     * Announces a message to all members in the channel.
     *
     * @param message the message to announce
     */
    void announce(@NotNull String message);
    
    @NotNull
    String applyFormat(@NotNull String format, @NotNull String originalFormat);
    
    @NotNull
    String applyFormat(@NotNull String format, @NotNull String originalFormat, @NotNull Player sender);
    
    void attachStorage(@NotNull ChannelStorage storage);
    
    boolean banMember(@NotNull Chatter chatter, boolean announce);
    
    void emote(@NotNull Chatter sender, @NotNull String message);
    
    @NotNull
    Set<UUID> getBansUUID();
    
    @NotNull
    Set<String> getBans();
    
    @NotNull
    ChatColor getColor();
    
    int getDistance();
    
    @NotNull
    String getFormat();
    
    @NotNull
    Set<Chatter> getMembers();
    
    @NotNull
    Set<UUID> getModeratorsUUID();

    @NotNull
    Set<String> getModerators();

    @NotNull
    Set<UUID> getMutesUUID();

    @NotNull
    Set<String> getMutes();
    
    @NotNull
    String getName();
    
    @Nullable
    String getNick();
    
    /**
     * Returns the channel password. If the channel has no password, {@code null} is returned.
     *
     * @return the password or {@code null}
     */
    @Nullable
    String getPassword();
    
    ChannelStorage getStorage();
    
    Set<String> getWorlds();
    
    boolean hasWorld(@NotNull String world);
    
    boolean hasWorld(@NotNull World world);
    
    boolean isBanned(@NotNull UUID name);
    
    boolean isCrossWorld();
    
    boolean isHidden();
    
    boolean isLocal();
    
    boolean isMember(@NotNull Chatter chatter);
    
    boolean isModerator(@NotNull UUID name);
    
    boolean isMuted(@NotNull UUID name);
    
    boolean isShortcutAllowed();
    
    boolean isTransient();
    
    boolean isVerbose();
    
    /**
     * Checks if this server is set to allow cross-server chat messages
     * @return true if cross-server is enabled
     */
    boolean isCrossServer();

    // SPAM
    
    public SpamTracker getSpamTracker();
    
    int getSpamMessageLimit();
    
    int getSpamMessageLimitBuildOff();
    
    public void setSpamMessageLimit(int limit);
    
    public void setSpamMessageLimitBuildOff(int buildOffTicks);
    
    /**
     * Kicks a chatter from the channel.
     *
     * @param chatter the chatter
     * @param announce whether the kick should be announced
     * @return {@code true} if the chatter could be removed, else {@code false} (for instance of the channel is a
     * private conversation and members can't be removed from it)
     */
    boolean kickMember(@NotNull Chatter chatter, boolean announce);
    
    void onFocusGain(@NotNull Chatter chatter);
    
    void onFocusLoss(@NotNull Chatter chatter);
    
    void processChat(@NotNull ChannelChatEvent event);
    
    boolean removeMember(@NotNull Chatter chatter, boolean announce, boolean flagUpdate);
    
    void removeWorld(@NotNull String world);
    
    void setBanned(@NotNull UUID id, boolean banned);
    
    void setBansUUID(@NotNull Set<UUID> bans);
    
    void setBans(@NotNull Set<String> bans);
    
    void setColor(@NotNull ChatColor color);
    
    void setCrossWorld(boolean crossWorld);
    
    /**
     * Sets the channel to allow transmitting across servers
     * @param crossServer
     * @param save
     */
    void setCrossServer(boolean crossServer, boolean save);

    void setDistance(int distance);
    
    void setFormat(@NotNull String format);
    
    void setModerator(@NotNull UUID id, boolean moderator);
    
    void setModeratorsUUID(@NotNull Set<UUID> moderators);

    void setModerators(@NotNull Set<String> moderators);
    
    void setMuted(@NotNull UUID id, boolean muted);
    
    void setMutesUUID(@NotNull Set<UUID> mutes);

    void setMutes(@NotNull Set<String> mutes);
    
    void setNick(@NotNull String nick);
    
    /**
     * Sets the channel password to the given value or removes it.
     *
     * @param password the new password or {@code null} if the password is to be removed
     */
    void setPassword(@Nullable String password);
    
    void setShortcutAllowed(boolean shortcutAllowed);
    
    void setVerbose(boolean verbose);
    
    void setWorlds(@NotNull Set<String> worlds);
    
    void setMuted(boolean value);
    
    boolean isMuted();
    
    /**
     * Sends an unprocessed, not logged message to all chatters in the channel.
     *
     * @param message the message
     */
    void sendRawMessage(@NotNull String message);
    
    MessageFormatSupplier getFormatSupplier();
    
    boolean addTag(@NotNull String tag, @NotNull TagFormatter formatter);
    
    class StandardChatter implements Chatter {
       
        private final Player player;
        private Chatter lastPMSource;
        private Channel activeChannel;
        private Channel lastActiveChannel;
        private Channel lastFocusableChannel;
        private ChatterStorage storage;
        private Set<Channel> channels = new HashSet<>();
        private Set<UUID> ignores = new HashSet<>();
        private String afkMessage = "";
        private boolean muted = false;
        private boolean afk = false;
        private boolean ignorePm = false;
        
        public StandardChatter(ChatterStorage storage, Player player) {
            this.storage = storage;
            this.player = player;
        }
        
        @Override
        public boolean addChannel(@NotNull Channel channel, boolean announce, boolean flagUpdate) {
            if (channels.contains(channel)) {
                return false;
            }
            
            channels.add(channel);
            if (!channel.isMember(this)) {
                channel.addMember(this, announce, flagUpdate);
            }
            
            if (flagUpdate) {
                storage.flagUpdate(this);
            }
            
            return true;
        }
        
        @Override
        public void attachStorage(ChatterStorage storage) {
            this.storage = storage;
        }
        
        @Override
        public Result canBan(@NotNull Channel channel) {
            if (Herochat.hasChannelPermission(player, channel, Permission.BAN)) {
                return Result.ALLOWED;
            }
            if (channel.isModerator(player.getUniqueId())
                && Herochat.getChannelManager().checkModPermission(Permission.BAN)) {
                return Result.ALLOWED;
            }
            
            return Result.NO_PERMISSION;
        }
        
        @Override
        public Result canColorMessages(@NotNull Channel channel, ChatColor color) {
            if (Herochat.hasChannelPermission(player, channel, Permission.COLOR)) {
                return Result.ALLOWED;
            }
            
            if (!Herochat.hasChannelPermission(player, channel, Permission.valueOf(color.name()))) {
                return Result.NO_PERMISSION;
            }
            
            return Result.ALLOWED;
        }
        
        @Override
        public Result canEmote(@NotNull Channel channel) {
            if (!channel.isMember(this)) {
                return Result.INVALID;
            }
            
            if (channel.isTransient() || !(Herochat.hasChannelPermission(player, channel, Permission.EMOTE))) {
                return Result.NO_PERMISSION;
            }
            
            if (muted || channel.isMuted(player.getUniqueId())) {
                return Result.MUTED;
            }
            
            if (!channel.hasWorld(player.getWorld())) {
                return Result.BAD_WORLD;
            }
            
            return Result.ALLOWED;
        }
        
        @Override
        public Result canFocus(@NotNull Channel channel) {
            if (!(Herochat.hasChannelPermission(player, channel, Permission.FOCUS))) {
                return Result.NO_PERMISSION;
            }
            Result speak = canSpeak(channel);
            if (speak != Result.ALLOWED && speak != Result.INVALID) {
                return Result.NO_PERMISSION;
            }
            return Result.ALLOWED;
        }
        
        @Override
        public Result canJoin(@NotNull Channel channel, @Nullable String password) {
            if (channel.isMember(this)) {
                return Result.INVALID;
            }
            
            if (!Herochat.hasChannelPermission(player, channel, Permission.JOIN)) {
                return Result.NO_PERMISSION;
            }
            
            if (channel.isBanned(player.getUniqueId())) {
                return Result.BANNED;
            }
            
            if (channel.getPassword() != null && !channel.getPassword().equals(password)) {
                return Result.BAD_PASSWORD;
            }
            
            return Result.ALLOWED;
        }
        
        @Override
        public Result canKick(@NotNull Channel channel) {
            if (Herochat.hasChannelPermission(player, channel, Permission.KICK)) {
                return Result.ALLOWED;
            }
            
            if (channel.isModerator(player.getUniqueId())
                && Herochat.getChannelManager().checkModPermission(Permission.KICK)) {
                return Result.ALLOWED;
            }
            
            return Result.NO_PERMISSION;
        }
        
        @Override
        public Result canLeave(@NotNull Channel channel) {
            if (!channel.isMember(this)) {
                return Result.INVALID;
            }
            
            if (!Herochat.hasChannelPermission(player, channel, Permission.LEAVE)) {
                return Result.NO_PERMISSION;
            }
            
            return Result.ALLOWED;
        }
        
        @Override
        public Result canModify(String setting, Channel channel) {
            setting = setting.toLowerCase();
            Permission permission;
            
            switch (setting) {
                case "nick":
                    permission = Permission.MODIFY_NICK;
                    break;
                case "format":
                    permission = Permission.MODIFY_FORMAT;
                    break;
                case "distance":
                    permission = Permission.MODIFY_DISTANCE;
                    break;
                case "color":
                    permission = Permission.MODIFY_COLOR;
                    break;
                case "shortcut":
                    permission = Permission.MODIFY_SHORTCUT;
                    break;
                case "password":
                    permission = Permission.MODIFY_PASSWORD;
                    break;
                case "verbose":
                    permission = Permission.MODIFY_VERBOSE;
                    break;
                case "chatcost":
                    permission = Permission.MODIFY_CHATCOST;
                    break;
                case "crossworld":
                    permission = Permission.MODIFY_CROSSWORLD;
                    break;
                case "focusable":
                    permission = Permission.MODIFY_FOCUSABLE;
                    break;
                default:
                    return Result.INVALID;
            }
            
            if (Herochat.hasChannelPermission(player, channel, permission)) {
                return Result.ALLOWED;
            }
            
            if (channel.isModerator(player.getUniqueId())
                && Herochat.getChannelManager().checkModPermission(permission)) {
                return Result.ALLOWED;
            }
            
            return Result.NO_PERMISSION;
        }
        
        @Override
        public Result canMute(@NotNull Channel channel) {
            if (Herochat.hasChannelPermission(player, channel, Permission.MUTE)) {
                return Result.ALLOWED;
            }
            
            if (channel.isModerator(player.getUniqueId())
                && Herochat.getChannelManager().checkModPermission(Permission.BAN)) {
                return Result.ALLOWED;
            }
            
            return Result.NO_PERMISSION;
        }
        
        @Override
        public Result canRemove(@NotNull Channel channel) {
            if (Herochat.hasChannelPermission(player, channel, Permission.REMOVE)) {
                return Result.ALLOWED;
            }
            
            if (channel.isModerator(player.getUniqueId())
                && Herochat.getChannelManager().checkModPermission(Permission.REMOVE)) {
                return Result.ALLOWED;
            }
            
            return Result.NO_PERMISSION;
        }
        
        @Override
        public Result canSpeak(@NotNull Channel channel) {
            if (!channel.isMember(this)) {
                return Result.INVALID;
            }
            
            if (!channel.isTransient() && !(Herochat.hasChannelPermission(player, channel, Permission.SPEAK))) {
                return Result.NO_PERMISSION;
            }
            
            if (muted || channel.isMuted(player.getUniqueId())) {
                return Result.MUTED;
            }
            
            if (!channel.hasWorld(player.getWorld())) {
                return Result.BAD_WORLD;
            }
            
            return Result.ALLOWED;
        }
        
        @Override
        public Result canViewInfo(@NotNull Channel channel) {
            if (!Herochat.hasChannelPermission(player, channel, Permission.INFO)) {
                return Result.NO_PERMISSION;
            }
            
            return Result.ALLOWED;
        }
        
        @Override
        public boolean equals(Object other) {
            return other == this
                || other instanceof Chatter && player.equals(((Chatter) other).getPlayer());
            
        }
        
        @Override
        public Channel getActiveChannel() {
            return activeChannel;
        }
        
        @Override
        public String getAFKMessage() {
            return afkMessage;
        }
        
        @Override
        public Set<Channel> getChannels() {
            return channels;
        }
        
        @Override
        public Set<UUID> getIgnores() {
            return ignores;
        }
        
        @Override
        public Channel getLastActiveChannel() {
            return lastActiveChannel;
        }
        
        @Override
        public Channel getLastFocusableChannel() {
            return lastFocusableChannel;
        }
        
        @Override
        public Chatter getLastPrivateMessageSource() {
            return lastPMSource;
        }
        
        @Override
        public String getName() {
            return player.getName();
        }
        
        @Override
        public Player getPlayer() {
            return player;
        }
        
        @Override
        public ChatterStorage getStorage() {
            return storage;
        }
        
        @Override
        public UUID getUniqueId() { return player.getUniqueId(); }
        
        @Override
        public boolean hasChannel(@NotNull Channel channel) {
            return channels.contains(channel);
        }
        
        @Override
        public int hashCode() {
            return player.hashCode();
        }
        
        @Override
        public boolean isAFK() {
            return afk;
        }
        
        @Override
        public boolean isIgnoring(Chatter other) {
            return canIgnore(other) == Result.ALLOWED && (isIgnoringPms() || isIgnoring(other.getUniqueId()));
        }
        
        @Override
        public boolean isIgnoring(UUID id) {
            return ignores.contains(id);
        }
        
        @Override
        public boolean isIgnoringPms() {
            return this.ignorePm;
        }
        
        @Override
        public boolean isInRange(Chatter other, int distance) {
            Player otherPlayer = other.getPlayer();
            return player.getWorld().equals(otherPlayer.getWorld())
                && player.getLocation().distanceSquared(otherPlayer.getLocation()) <= distance * distance;
        }
        
        @Override
        public boolean isMuted() {
            return muted;
        }
        
        @Override
        public boolean removeChannel(@NotNull Channel channel, boolean announce, boolean flagUpdate) {
            if (!channels.contains(channel)) {
                return false;
            }
            
            channels.remove(channel);
            if (channel.isMember(this)) {
                channel.removeMember(this, announce, flagUpdate);
            }
            
            if (flagUpdate) {
                storage.flagUpdate(this);
            }
            
            return true;
        }
        
        @Override
        public void setActiveChannel(@Nullable Channel channel, boolean announce, boolean flagUpdate) {
            if (channel != null && channel.equals(activeChannel)) {
                return;
            }
            
            if (activeChannel != null) {
                activeChannel.onFocusLoss(this);
            }
            
            if (activeChannel != null && !activeChannel.isTransient()) {
                lastActiveChannel = activeChannel;
                if (canFocus(activeChannel) == Result.ALLOWED) {
                    lastFocusableChannel = activeChannel;
                }
            }
            activeChannel = channel;
            if (activeChannel != null) {
                activeChannel.onFocusGain(this);
                
                if (announce) {
                    try {
                        Messaging.send(player, Herochat.getMessage("chatter_focus"),
                            channel.getColor() + channel.getName());
                    } catch (MessageNotFoundException e) {
                        Herochat.severe(e.getMessage());
                    }
                }
            }
            
            if (flagUpdate) {
                storage.flagUpdate(this);
            }
        }
        
        @Override
        public void setAFK(boolean afk) {
            this.afk = afk;
        }
        
        @Override
        public void setAFKMessage(String message) {
            this.afkMessage = message;
        }
        
        @Override
        public void setIgnore(UUID id, boolean ignore, boolean flagUpdate) {
            if (ignore) {
                ignores.add(id);
            }
            else {
                ignores.remove(id);
            }
            
            if (flagUpdate) {
                storage.flagUpdate(this);
            }
        }
        
        @Override
        public void setLastPrivateMessageSource(Chatter chatter) {
            this.lastPMSource = chatter;
        }
        
        @Override
        public void setMuted(boolean muted, boolean flagUpdate) {
            this.muted = muted;
            
            if (flagUpdate) {
                storage.flagUpdate(this);
            }
        }
        
        @Override
        public void setIgnoringPms(boolean ignoring, boolean flagUpdate) {
            this.ignorePm = ignoring;
            
            if (flagUpdate) {
                storage.flagUpdate(this);
            }
        }
        
        @Override
        public boolean shouldAutoJoin(@NotNull Channel channel) {
            return Herochat.hasChannelPermission(player, channel, Permission.AUTOJOIN);
        }
        
        @Override
        public boolean shouldForceJoin(@NotNull Channel channel) {
            return Herochat.hasChannelPermission(player, channel, Permission.FORCE_JOIN);
        }
        
        @Override
        public boolean shouldForceLeave(@NotNull Channel channel) {
            return Herochat.hasChannelPermission(player, channel, Permission.FORCE_LEAVE);
        }
        
        @Override
        public void refocus() {
            // Don't do anything if we can focus the current channel anyway.
            if (activeChannel != null) {
                if (canFocus(activeChannel) == Result.ALLOWED) {
                    return;
                }
                lastActiveChannel = activeChannel;
                lastActiveChannel.onFocusLoss(this);
                activeChannel = null;
            }
            for (Channel channel : this.getChannels()) {
                if (canFocus(channel) == Result.ALLOWED) {
                    activeChannel = channel;
                    activeChannel.onFocusGain(this);
                    try {
                        Messaging.send(player, Herochat.getMessage("chatter_focus"),
                            channel.getColor() + channel.getName());
                    } catch (MessageNotFoundException e) {
                        Herochat.severe(e.getMessage());
                    }
                    break;
                }
            }
        }
        
        @Override
        public Result canIgnore(Chatter other) {
            return other.getPlayer().hasPermission("herochat.admin.unignore")? Result.NO_PERMISSION : Result.ALLOWED;
        }
        
        @Override
        public void disconnect() {
            Iterator<Channel> iter = channels.iterator();
            while (iter.hasNext()) {
                Channel channel = iter.next();
                iter.remove();
                channel.removeMember(this, false, false);
            }
        }
    }
    
}
