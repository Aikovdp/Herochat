package com.dthielke.herochat;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SpamTracker {
    
    private final Map<UUID, Entry> counterMap = new HashMap<>();
    
    private int limit, buildOff;
    
    // ACTIONS
    
    public SpamTracker(int limit, int buildOff) {
        setLimit(limit);
        setBuildOff(buildOff);
    }
    
    public SpamTracker(SpamTracker copyOf) {
        this.limit = copyOf.limit;
        this.buildOff = copyOf.buildOff;
    }
    
    public SpamTracker() {
        this(1, 1000);
    }
    
    public boolean onChat(UUID uuid) {
        if (!counterMap.containsKey(uuid)) {
            counterMap.put(uuid, new Entry(System.currentTimeMillis(), 1));
            return true;
        }
        
        long now = System.currentTimeMillis();
        Entry entry = counterMap.get(uuid);
        entry.counter++;
        int reduction = (int) (now - entry.timestamp) / buildOff;
        entry.counter = Math.max(1, entry.counter - reduction);
        entry.timestamp = now;
        
        if (entry.counter > this.limit) {
            entry.counter = this.limit;
            return false;
        }
        else return true;
    }
    
    // GETTERS
    
    /**
     * Returns the message limit for, before messages are caught by the spam filter.
     *
     * @return the message limit
     */
    public int getLimit() {
        return limit;
    }
    
    /**
     * Returns the amount of milliseconds between each message rate build-off.
     *
     * @return the amount of milliseconds between limit build-offs
     */
    public int getBuildOff() {
        return buildOff;
    }
    
    // SETTERS
    
    public void setLimit(int limit) {
        if (limit < 1)
            throw new IllegalArgumentException("limit must be at least 1");
        this.limit = limit;
    }
    
    public void setBuildOff(int buildOffMillis) {
        if (buildOffMillis < 1)
            throw new IllegalArgumentException("build-off millis must be at least 1");
        this.buildOff = buildOffMillis;
    }
    
    // MISC
    
    @Override
    public String toString() {
        return SpamTracker.class.getSimpleName() + "{limit: " + limit + ", buildoff: " + buildOff + "ms}";
    }
    
    @Override
    public final SpamTracker clone() {
        return new SpamTracker(this);
    }
    
    // STRUCTS
    
    private static class Entry {
        
        private long timestamp;
        private int counter;
        
        public Entry(long timestamp, int counter) {
            this.timestamp = timestamp;
            this.counter = counter;
        }
    }
    
}
