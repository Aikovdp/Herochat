/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.dthielke.herochat.command.BasicCommand;
import com.dthielke.herochat.MessageNotFoundException;
import com.dthielke.herochat.persist.YMLChannelStorage;
import com.dthielke.herochat.persist.YMLChatterStorage;
import net.milkbowl.vault.chat.Chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.dthielke.herochat.command.CommandHandler;
import com.dthielke.herochat.command.commands.AFKCommand;
import com.dthielke.herochat.command.commands.BanCommand;
import com.dthielke.herochat.command.commands.CreateCommand;
import com.dthielke.herochat.command.commands.EmoteCommand;
import com.dthielke.herochat.command.commands.FocusCommand;
import com.dthielke.herochat.command.commands.HelpCommand;
import com.dthielke.herochat.command.commands.IgnoreCommand;
import com.dthielke.herochat.command.commands.IgnorePMCommand;
import com.dthielke.herochat.command.commands.InfoCommand;
import com.dthielke.herochat.command.commands.JoinCommand;
import com.dthielke.herochat.command.commands.KickCommand;
import com.dthielke.herochat.command.commands.LeaveCommand;
import com.dthielke.herochat.command.commands.ListCommand;
import com.dthielke.herochat.command.commands.ModCommand;
import com.dthielke.herochat.command.commands.MsgCommand;
import com.dthielke.herochat.command.commands.MuteCommand;
import com.dthielke.herochat.command.commands.QuickMsgCommand;
import com.dthielke.herochat.command.commands.ReloadCommand;
import com.dthielke.herochat.command.commands.RemoveCommand;
import com.dthielke.herochat.command.commands.ReplyCommand;
import com.dthielke.herochat.command.commands.SaveCommand;
import com.dthielke.herochat.command.commands.SetCommand;
import com.dthielke.herochat.command.commands.WhoCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * The Herochat plugin class.
 */
public class Herochat extends JavaPlugin {
    
    // const loggers
    private static final Logger LOG = Logger.getLogger("Minecraft");
    private static final Logger CHAT_LOG = Logger.getLogger("Herochat");
    
    // const handlers
    private static final CommandHandler COMMAND_HANDLER = new CommandHandler();
    private static final MessageHandler MESSAGE_HANDLER = new MessageHandler();
    
    // const managers
    private static final ChannelManager CHANNEL_MANAGER = new ChannelManager();
    private static final ChatterManager CHATTER_MANAGER = new ChatterManager();
    private static final ConfigManager CONFIG_MANAGER = new ConfigManager();
    
    // etc
    private static Chat chatService;
    private static Herochat plugin;
    private static ResourceBundle messages;
    private static boolean chatLogEnabled;
    private static boolean logToBukkit;
    private static boolean colorLogs;
    private static boolean colorBukkitLogs;
    private static boolean bungeeEnabled;
    
    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] args) {
        return COMMAND_HANDLER.dispatch(sender, label, args);
    }
    
    // ON ENABLE
    
    @Override
    public void onEnable() {
        plugin = this;
        
        setupStorage();
        setupChatService();
        CHANNEL_MANAGER.loadChannels();
        
        try {
            CONFIG_MANAGER.load(new File(getDataFolder(), "config.yml"));
        } catch (IOException e) {
            // If we had an exception thrown here that means we weren't able to load a translation, so just disable the
            // plugin.
            severe("An error occurred when loading the config. Herochat will be disabled.");
            e.printStackTrace();
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }
        
        CHANNEL_MANAGER.getStorage().update();
        setupChatLog();
        for (Player player : getServer().getOnlinePlayers()) {
            CHATTER_MANAGER.addChatter(player);
        }
        
        registerCommands();
        registerEvents();
        
        info("Version " + getDescription().getVersion() + " is enabled.");
    }
    
    /**
     * Called {@link #onEnable()}.
     */
    public void setupStorage() {
        File channelFolder = new File(getDataFolder(), "channels");
        if (!channelFolder.isDirectory() && !channelFolder.mkdirs()) {
            throw new IllegalStateException(channelFolder + " must be an existing directory");
        }
        ChannelStorage channelStorage = new YMLChannelStorage(channelFolder);
        CHANNEL_MANAGER.setStorage(channelStorage);
        
        File chatterFolder = new File(getDataFolder(), "chatters");
        if (!chatterFolder.isDirectory() && !chatterFolder.mkdirs()) {
            throw new IllegalStateException(channelFolder + " must be an existing directory");
        }
        ChatterStorage chatterStorage = new YMLChatterStorage(chatterFolder);
        CHATTER_MANAGER.setStorage(chatterStorage);
    }
    
    /**
     * Called {@link #onEnable()}.
     */
    private boolean setupChatService() {
        RegisteredServiceProvider<Chat> svc = getServer().getServicesManager().getRegistration(Chat.class);
        if (svc != null) {
            chatService = svc.getProvider();
            return true;
        }
        return false;
    }
    
    /**
     * Called {@link #onEnable()}.
     */
    private void registerCommands() {
        BasicCommand[] commands = new BasicCommand[] {
            new FocusCommand(),
            new JoinCommand(),
            new LeaveCommand(),
            new QuickMsgCommand(),
            new IgnoreCommand(),
            new IgnorePMCommand(),
            new MsgCommand(),
            new ReplyCommand(),
            new ListCommand(),
            new WhoCommand(),
            new AFKCommand(),
            new EmoteCommand(),
            new CreateCommand(),
            new RemoveCommand(),
            new SetCommand(),
            new InfoCommand(),
            new MuteCommand(),
            new KickCommand(),
            new BanCommand(),
            new ModCommand(),
            new SaveCommand(),
            new ReloadCommand(),
            new HelpCommand()
        };
        for (BasicCommand command : commands)
            COMMAND_HANDLER.addCommand(command);
    }
    
    /**
     * Called {@link #onEnable()}.
     */
    private void registerEvents() {
        PluginManager pm = this.getServer().getPluginManager();
        HCPlayerListener pcl = new HCPlayerListener(this);
        pm.registerEvents(pcl, this);
        if (isBungeeEnabled()) {
            new CrossServerListener(this.getConfig().getStringList("servers"));
        }
    }
    
    /**
     * Called {@link #onEnable()}.
     */
    private void setupChatLog() {
        if (chatLogEnabled) {
            CHAT_LOG.setLevel(Level.INFO);
            CHAT_LOG.setParent(LOG);
            CHAT_LOG.setUseParentHandlers(logToBukkit);
            File logDir = new File(getDataFolder(), "logs");
            logDir.mkdirs();
            String filename = logDir.getAbsolutePath() + "/chat.%g.%u.log";
            try {
                FileHandler chatLogHandler = new FileHandler(filename, 512 * 1024, 1000, true);
                chatLogHandler.setFormatter(new ChatLogFormatter(Herochat.colorLogs));
                CHAT_LOG.addHandler(chatLogHandler);
            } catch (IOException e) {
                warning("Failed to create chat log handler.");
                e.printStackTrace();
            }
        }
    }
    
    // ON DISABLE
    
    @Override
    public void onDisable() {
        if (CHANNEL_MANAGER.getStorage() != null)
            CHANNEL_MANAGER.getStorage().update();
        
        if (CHATTER_MANAGER.getStorage() != null)
            CHATTER_MANAGER.getStorage().update();
        
        info("Version " + getDescription().getVersion() + " is disabled.");
    }
    
    // GETTERS
    
    @NotNull
    public static Herochat getPlugin() {
        return plugin;
    }
    
    @NotNull
    public static ChannelManager getChannelManager() {
        return CHANNEL_MANAGER;
    }
    
    @NotNull
    public static ChatterManager getChatterManager() {
        return CHATTER_MANAGER;
    }
    
    @NotNull
    public static CommandHandler getCommandHandler() {
        return COMMAND_HANDLER;
    }
    
    @NotNull
    public static ConfigManager getConfigManager() {
        return CONFIG_MANAGER;
    }
    
    @Nullable
    public static Chat getChatService() {
        return chatService;
    }
    
    /**
     * Returns the localized translation of a given message key.
     *
     * @param key the message key
     * @return the translation
     * @throws MessageNotFoundException if no message with the given key could be found
     */
    public static String getMessage(@NotNull String key) throws MessageNotFoundException {
        try {
            return messages.getString(key);
        } catch (MissingResourceException ex) {
            throw new MessageNotFoundException(key);
        }
    }
    
    /**
     * Returns Herochat's message handler.
     *
     * @return the message handler
     */
    public static MessageHandler getMessageHandler() {
        return MESSAGE_HANDLER;
    }
    
    public static boolean hasChannelPermission(CommandSender user, Channel channel, Chatter.Permission permission) {
        String formedPermission = permission.form(channel).toLowerCase();
        return user.isPermissionSet(formedPermission)?
            user.hasPermission(formedPermission) :
            user.hasPermission(permission.formAll());
    }
    
    public static void setLocale(Locale locale) throws ClassNotFoundException {
        messages = ResourceBundle.getBundle("com.dthielke.herochat.resources.Messages", locale);
        if (messages == null) {
            throw new ClassNotFoundException("com.dthielke.herochat.resources.Messages");
        }
    }
    
    // CONSOLE LOGGING
    
    public static void info(String message) {
        LOG.info("[Herochat] " + message);
    }
    
    public static void severe(String message) {
        LOG.severe("[Herochat] " + message);
    }
    
    public static void warning(String message) {
        LOG.warning("[Herochat] " + message);
    }
    
    // CHAT LOGGING
    
    public static void logChat(String message) {
        if (chatLogEnabled) {
            CHAT_LOG.info(colorBukkitLogs? message : ChatColor.stripColor(message));
        }
    }
    
    public static void setChatLogEnabled(boolean chatLogEnabled) {
        Herochat.chatLogEnabled = chatLogEnabled;
    }
    
    public static void setLogToBukkitEnabled(boolean enabled) {
        Herochat.logToBukkit = enabled;
    }
    
    public static void setColorLogging(boolean enabled) {
        Herochat.colorLogs = enabled;
    }
    
    public static void setColorBukkitLogging(boolean enabled) {
        Herochat.colorBukkitLogs = enabled;
    }

    // BUNGEECORD

    public static void setBungeeEnabled(boolean enabled) {
        Herochat.bungeeEnabled = enabled;
    }

    public boolean isBungeeEnabled() {
        return Herochat.bungeeEnabled;
    }
}
