/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.command;

import com.dthielke.herochat.Herochat;
import com.dthielke.herochat.MessageNotFoundException;

import org.bukkit.command.CommandSender;

public abstract class BasicCommand implements Command {
    
    private final String name;
    private String
        description = "",
        usage = "",
        permission = "";
    private String[]
        notes = new String[0],
        identifiers = new String[0];
    private int
        minArguments = 0,
        maxArguments = 0;
    
    public BasicCommand(String name) {
        this.name = name;
    }
    
    /**
     * Convenience method for obtaining a message via {@link Herochat#getMessage(String)} and failing silently.
     *
     * @param key the message key
     * @return the message translation
     */
    protected String getMessage(String key) {
        try {
            return Herochat.getMessage(key);
        } catch (MessageNotFoundException e) {
            Herochat.severe(e.getMessage());
            return "MISSING(\"" + key + "\")";
        }
    }
    
    @Override
    public void cancelInteraction(CommandSender executor) {}
    
    @Override
    public String getDescription() {
        return description;
    }
    
    @Override
    public String[] getIdentifiers() {
        return identifiers;
    }
    
    @Override
    public int getMaxArguments() {
        return maxArguments;
    }
    
    @Override
    public int getMinArguments() {
        return minArguments;
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    @Override
    public String[] getNotes() {
        return notes;
    }
    
    @Override
    public String getPermission() {
        return permission;
    }
    
    @Override
    public String getUsage() {
        return usage;
    }
    
    @Override
    public boolean isIdentifier(CommandSender executor, String input) {
        for (String identifier : identifiers)
            if (input.equalsIgnoreCase(identifier))
                return true;
        return false;
    }
    
    @Override
    public boolean isInProgress(CommandSender executor) {
        return false;
    }
    
    @Override
    public boolean isInteractive() {
        return false;
    }
    
    @Override
    public boolean isShownOnHelpMenu() {
        return true;
    }
    
    /**
     * Sets the range of allowed arguments.
     *
     * @param min the minimum of the range
     * @param max the maximum of the range
     * @throws IllegalArgumentException if min or max are negative or if {@code max < min}.
     */
    public void setArgumentRange(int min, int max) {
        if (min < 0) throw new IllegalArgumentException("min must be positive");
        if (max < 0) throw new IllegalArgumentException("max must be positive");
        if (max < min) throw new IllegalArgumentException("max must be >= min");
        this.minArguments = min;
        this.maxArguments = max;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public void setIdentifiers(String... identifiers) {
        this.identifiers = identifiers;
    }
    
    public void setNotes(String... notes) {
        this.notes = notes;
    }
    
    public void setPermission(String permission) {
        this.permission = permission;
    }
    
    public void setUsage(String usage) {
        this.usage = usage;
    }
    
}
