/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.command.commands;

import com.dthielke.herochat.Chatter;
import com.dthielke.herochat.Herochat;
import com.dthielke.herochat.command.BasicCommand;
import com.dthielke.herochat.util.Messaging;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class IgnorePMCommand extends BasicCommand {
    
    public IgnorePMCommand() {
        super("IgnorePM");
        setDescription(getMessage("command_ignorepm"));
        setUsage("/ch ignorepm");
        setArgumentRange(0, 1);
        setIdentifiers("ignorepm", "herochat ignorepm");
    }
    
    @Override
    public boolean execute(CommandSender sender, String identifier, String[] args) {
        if (!(sender instanceof Player)) return true;
        
        Player player = (Player) sender;
        Chatter chatter = Herochat.getChatterManager().getChatter(player);
        
        chatter.setIgnoringPms(chatter.isIgnoringPms(), true);
        if (chatter.isIgnoringPms()) {
            Messaging.send(sender, getMessage("ignore_allPm"));
        }
        else {
            Messaging.send(sender, getMessage("ignore_offPm"));
        }
        
        return true;
    }
    
}
