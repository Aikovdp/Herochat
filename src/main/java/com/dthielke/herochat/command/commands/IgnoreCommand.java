/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.command.commands;

import com.dthielke.herochat.Chatter;
import com.dthielke.herochat.Herochat;
import com.dthielke.herochat.command.BasicCommand;
import com.dthielke.herochat.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class IgnoreCommand extends BasicCommand {
    
    public IgnoreCommand() {
        super("Ignore");
        setDescription(getMessage("command_ignore"));
        setUsage("/ch ignore " + ChatColor.DARK_GRAY + "[player]");
        setArgumentRange(0, 1);
        setIdentifiers("ignore", "ch ignore", "herochat ignore");
    }
    
    @SuppressWarnings("deprecation")
    @Override
    public boolean execute(CommandSender sender, String identifier, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }
        
        Player player = (Player) sender;
        Chatter chatter = Herochat.getChatterManager().getChatter(player);
        
        if (args.length == 0) {
            StringBuilder msg = new StringBuilder(getMessage("ignore_listHead"));
            if (chatter.getIgnores().isEmpty()) {
                msg.append(" ").append(getMessage("ignore_listEmpty"));
            }
            else for (UUID id : chatter.getIgnores()) {
                msg.append(" ").append(id);
            }
            Messaging.send(sender, msg.toString());
        }
        else {
            String targetName = args[args.length - 1];
            OfflinePlayer targetPlayer = Bukkit.getServer().getOfflinePlayer(targetName);
            UUID targetId;
            
            if (targetPlayer != null) {
                targetId = targetPlayer.getUniqueId();
            }
            else {
                Messaging.send(sender, getMessage("ignore_noPlayer"), targetName);
                return true;
            }
            
            if (chatter.isIgnoring(targetId)) {
                chatter.setIgnore(targetId, false, true);
                Messaging.send(sender, getMessage("ignore_confirmUnignore"), targetName);
            }
            else {
                chatter.setIgnore(targetId, true, true);
                Messaging.send(sender, getMessage("ignore_confirmIgnore"), targetName);
            }
        }
        
        return true;
    }
    
}
