/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.command.commands;

import com.dthielke.herochat.Channel;
import com.dthielke.herochat.ChannelManager;
import com.dthielke.herochat.Herochat;
import com.dthielke.herochat.StandardChannel;
import com.dthielke.herochat.command.BasicCommand;
import com.dthielke.herochat.util.Messaging;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CreateCommand extends BasicCommand {
    
    public CreateCommand() {
        super("Create Channel");
        setDescription(getMessage("command_create"));
        setUsage("/ch create " + ChatColor.DARK_GRAY + "<name >[nick] ");
        setArgumentRange(1, 2);
        setIdentifiers("ch create", "herochat create");
        setPermission("herochat.create");
    }
    
    @Override
    public boolean execute(CommandSender sender, String identifier, String[] args) {
        String name = args[0];
        ChannelManager channelMan = Herochat.getChannelManager();
        if (channelMan.hasChannel(name)) {
            Messaging.send(sender, getMessage("create_nameTaken"));
            return true;
        }
        else if (!name.matches("[a-zA-Z0-9]+")) {
            Messaging.send(sender, getMessage("create_nameInvalid"));
            return true;
        }
        
        String nick;
        if (args.length == 2) {
            nick = args[1];
            if (!nick.matches("[a-zA-Z0-9]+")) {
                Messaging.send(sender, getMessage("create_nickInvalid"));
                return true;
            }
        }
        else {
            nick = name;
            for (int i = 0; i < name.length(); i++) {
                nick = name.substring(0, i + 1);
                if (!channelMan.hasChannel(nick)) {
                    break;
                }
            }
        }
        
        if (channelMan.hasChannel(nick)) {
            Messaging.send(sender, "create_nickTaken");
            return true;
        }
        
        Channel channel = new StandardChannel(channelMan.getStorage(), name, nick, channelMan);
        if (sender instanceof Player) {
            channel.setModerator(((Player) sender).getUniqueId(), true);
        }
        channelMan.addChannel(channel);
        channelMan.getStorage().update(channel);
        Messaging.send(sender, getMessage("create_confirm"));
        
        return true;
    }
    
}
