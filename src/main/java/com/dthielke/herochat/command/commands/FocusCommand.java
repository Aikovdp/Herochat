/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.command.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dthielke.herochat.Channel;
import com.dthielke.herochat.ChannelManager;
import com.dthielke.herochat.Chatter;
import com.dthielke.herochat.Chatter.Result;
import com.dthielke.herochat.ConversationChannel;
import com.dthielke.herochat.Herochat;
import com.dthielke.herochat.command.BasicCommand;
import com.dthielke.herochat.util.Messaging;

public class FocusCommand extends BasicCommand {
    
    public FocusCommand() {
        super("Focus");
        setDescription(getMessage("command_focus"));
        setUsage("/ch " + ChatColor.DARK_GRAY + "<channel> [password]");
        setArgumentRange(1, 2);
        setIdentifiers("ch", "herochat");
    }
    
    @Override
    public boolean execute(CommandSender sender, String identifier, String[] args) {
        if (!(sender instanceof Player)) return true;
        
        Player player = (Player) sender;
        
        ChannelManager channelMngr = Herochat.getChannelManager();
        Channel channel = channelMngr.getChannel(args[0]);
        if (channel == null) {
            Messaging.send(sender, getMessage("focus_noChannel"));
            return true;
        }
        
        String password = "";
        if (args.length == 2) {
            password = args[1];
        }
        
        Chatter chatter = Herochat.getChatterManager().getChatter(player);
        
        if (chatter.canFocus(channel) == Result.NO_PERMISSION) {
            Messaging.send(sender, getMessage("focus_noPermission"), channel.getColor() + channel.getName());
            return true;
        }
        
        if (!chatter.hasChannel(channel)) {
            Result result = chatter.canJoin(channel, password);
            switch (result) {
                case NO_PERMISSION:
                    Messaging.send(sender, getMessage("join_noPermission"), channel.getColor() + channel.getName());
                    return true;
                case BANNED:
                    Messaging.send(sender, getMessage("focus_banned"), channel.getColor() + channel.getName());
                    return true;
                case BAD_PASSWORD:
                    Messaging.send(sender, getMessage("focus_badPassword"));
                    return true;
                default:
            }
            
            channel.addMember(chatter, true, true);
            Messaging.send(player, getMessage("focus_confirm"), channel.getColor() + channel.getName());
        }
        
        chatter.setActiveChannel(channel, true, true);
        
        // cleanup conversation channels if no one has them active
        Channel lastChannel = chatter.getLastActiveChannel();
        if (lastChannel instanceof ConversationChannel) {
            for (Chatter otherChatter : lastChannel.getMembers()) {
                if (!otherChatter.equals(chatter)) {
                    if (!otherChatter.getActiveChannel().equals(lastChannel)) {
                        lastChannel.removeMember(chatter, false, true);
                        lastChannel.removeMember(otherChatter, false, true);
                    }
                }
            }
        }
        
        return true;
    }
    
}
