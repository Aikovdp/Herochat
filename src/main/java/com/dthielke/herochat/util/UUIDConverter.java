package com.dthielke.herochat.util;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.*;

public final class UUIDConverter {
    
    private UUIDConverter() {}
    
    @SuppressWarnings("deprecation")
    public static Set<UUID> stringToUuid(Collection<String> strings) {
        Set<UUID> ids = new HashSet<>();
        for (String s : strings) {
            try {
                ids.add(UUID.fromString(s));
            } catch (IllegalArgumentException e) {
                OfflinePlayer player = Bukkit.getServer().getOfflinePlayer(s);
                if (player != null) {
                    ids.add(player.getUniqueId());
                }
            }
        }
        
        return ids;
    }
    
    /**
     * Parses the UUID-string if the string is a valid UUID. If not, this method attempts to find the player with a name
     * matching the input and returns their UUID.
     * <p>
     * If no player with the name is found, a random UUID is returned.
     *
     * @param string the uuid or player name
     * @return the uuid
     */
    @NotNull
    @SuppressWarnings("deprecation")
    public static UUID stringToUuid(@NotNull String string) {
        try {
            return UUID.fromString(string);
        } catch (IllegalArgumentException e) {
            OfflinePlayer player = Bukkit.getServer().getOfflinePlayer(string);
            if (player != null) {
                return player.getUniqueId();
            }
        }
        return UUID.randomUUID();
    }
    
    public static Set<String> uuidToString(Collection<UUID> ids) {
        Set<String> strings = new HashSet<>();
        for (UUID id : ids) {
            strings.add(id.toString());
        }
        
        return strings;
    }
    
    public static String uuidToString(UUID id) {
        return id.toString();
    }
    
    public static File fileConvertToUuidFormat(String name, String id, File folder, File created) {
        for (File f : folder.listFiles()) {
            if (f.getName().contains(name)) {
                FileConfiguration config = new YamlConfiguration();
                f.renameTo(created);
                try {
                    File new_f = new File(created.getPath());
                    config.load(new_f);
                    config.set("UUID", id);
                    config.save(new_f);
                } catch (IOException | InvalidConfigurationException e) {
                    e.printStackTrace();
                }
                return created;
            }
        }
        return null;
    }
    
    @SuppressWarnings("ConstantConditions")
    public static File uuidChange(String name, String id, File chatterFolder, File created) {
        for (File folder : chatterFolder.listFiles()) {
            if (folder.isDirectory()) {
                for (File f : folder.listFiles()) {
                    if (f.getName().contains(id)) {
                        FileConfiguration config = new YamlConfiguration();
                        f.renameTo(created);
                        try {
                            File new_f = new File(created.getPath());
                            config.load(new_f);
                            config.set("name", name);
                            config.save(new_f);
                        } catch (IOException | InvalidConfigurationException e) {
                            e.printStackTrace();
                        }
                        return created;
                    }
                }
            }
        }
        return null;
    }
    
}
