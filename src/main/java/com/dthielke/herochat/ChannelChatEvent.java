/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat;

import com.dthielke.herochat.Channel;
import com.dthielke.herochat.Chatter;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Called by the {@link com.dthielke.herochat.MessageHandler} whenever a {@link Chatter} chats in a {@link Channel}.
 */
public class ChannelChatEvent extends Event {
    
    private static final HandlerList handlers = new HandlerList();
    private final Chatter sender;
    private Channel channel;
    private Chatter.Result result;
    private String msg;
    private String format;
    private String bukkitFormat;
    
    public ChannelChatEvent(Chatter sender, Channel channel, Chatter.Result result, String msg, String bukkitFormat,
                               String channelFormat) {
        this.sender = sender;
        this.channel = channel;
        this.result = result;
        this.msg = msg;
        this.format = channelFormat;
        this.bukkitFormat = bukkitFormat;
    }
    
    public String getMessage() {
        return msg;
    }
    
    public void setMessage(String msg) {
        this.msg = msg;
    }
    
    public String getFormat() {
        return format;
    }
    
    public void setFormat(String format) {
        this.format = format;
    }
    
    public String getBukkitFormat() {
        return bukkitFormat;
    }
    
    public void setBukkitFormat(String bukkitFormat) {
        this.bukkitFormat = bukkitFormat;
    }
    
    public Channel getChannel() {
        return channel;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public Chatter.Result getResult() {
        return result;
    }
    
    public Chatter getSender() {
        return sender;
    }
    
    public void setChannel(Channel channel) {
        if (channel != null) {
            this.channel = channel;
        }
    }
    
    public void setResult(Chatter.Result result) {
        this.result = result;
    }
    
    /**
     * To be called by Bukkit event handling reflections.
     *
     * @return the handler list
     */
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
}
