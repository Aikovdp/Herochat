/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.persist;

import com.dthielke.herochat.Channel;
import com.dthielke.herochat.ChannelStorage;
import com.dthielke.herochat.Herochat;
import com.dthielke.herochat.StandardChannel;
import com.dthielke.herochat.util.Messaging;
import com.dthielke.herochat.util.UUIDConverter;
import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.*;

public class YMLChannelStorage implements ChannelStorage {
    
    private Map<Channel, FileConfiguration> configs = new HashMap<>();
    private Set<Channel> updates = new HashSet<>();
    private final File channelFolder;
    
    public YMLChannelStorage(@NotNull File channelFolder) {
        this.channelFolder = channelFolder;
    }
    
    @Override
    public void addChannel(Channel channel) {
        if (configs.containsKey(channel) || channel.isTransient()) {
            return;
        }
        File file = new File(channelFolder, channel.getName() + ".yml");
        FileConfiguration config = new YamlConfiguration();
        try {
            if (file.exists()) {
                config.load(file);
            }
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
        configs.put(channel, config);
        flagUpdate(channel);
    }
    
    @Override
    public void flagUpdate(Channel channel) {
        if (!channel.isTransient()) {
            updates.add(channel);
        }
    }
    
    @Override
    public Channel load(String name) {
        File file = new File(channelFolder, name + ".yml");
        FileConfiguration config = new YamlConfiguration();
        try {
            config.load(file);
        } catch (IOException e) {
            Herochat.severe("Could not open file " + file.getName());
            e.printStackTrace();
        } catch (InvalidConfigurationException e) {
            Herochat.severe("Could not load channel " + file.getName());
            e.printStackTrace();
        }
        
        String nick = config.getString("nick", name);
        String format = config.getString("format", "{default}");
        String password = config.getString("password", "");
        ChatColor color = Messaging.parseColor(config.getString("color", "WHITE"));
        if (color == null) {
            Herochat.warning("The color '" + config.getString("color") + "' is not valid.");
            color = ChatColor.WHITE;
        }
        int distance = config.getInt("distance", 0);
        boolean shortcut;
        if (config.contains("shortcutAllowed")) {
            shortcut = config.getBoolean("shortcutAllowed", false);
        }
        else {
            shortcut = config.getBoolean("shortcut", false);
        }
        boolean verbose = config.getBoolean("verbose", true);
        boolean crossWorld = config.getBoolean("crossworld", true);
        boolean muted = config.getBoolean("muted", false);
        int messageLimit = config.getInt("spam.message-limit", 3);
        int messageLimitBuildOff = config.getInt("spam.message-limit-buildoff", 20);
        boolean crossServer = config.getBoolean("cross-server", false);
        config.addDefault("worlds", new ArrayList<String>());
        config.addDefault("bans", new ArrayList<String>());
        config.addDefault("mutes", new ArrayList<String>());
        config.addDefault("moderators", new ArrayList<String>());
        Set<String> worlds = new HashSet<>(config.getStringList("worlds"));
        Set<UUID>
            bans = UUIDConverter.stringToUuid(config.getStringList("bans")),
            mutes = UUIDConverter.stringToUuid(config.getStringList("mutes")),
            moderators = UUIDConverter.stringToUuid(config.getStringList("moderators"));
        config.set("bans", UUIDConverter.uuidToString(bans));
        config.set("mutes", UUIDConverter.uuidToString(mutes));
        config.set("moderators", UUIDConverter.uuidToString(moderators));
        try {
            config.save(file);
        } catch (IOException e) {
            Herochat.severe("Could not save file " + file.getName());
            e.printStackTrace();
        }
        
        Channel channel = new StandardChannel(this, name, nick, Herochat.getChannelManager());
        channel.setFormat(format);
        channel.setPassword(password);
        channel.setColor(color);
        channel.setDistance(distance);
        channel.setShortcutAllowed(shortcut);
        channel.setVerbose(verbose);
        channel.setMuted(muted);
        channel.setCrossWorld(crossWorld);
        channel.setCrossServer(crossServer, false);
        channel.setWorlds(worlds);
        channel.setBansUUID(bans);
        channel.setMutesUUID(mutes);
        channel.setModeratorsUUID(moderators);
        channel.setSpamMessageLimit(messageLimit);
        channel.setSpamMessageLimitBuildOff(messageLimitBuildOff);
        addChannel(channel);
        return channel;
    }
    
    @Override
    public Set<Channel> loadChannels() {
        Set<Channel> channels = new HashSet<>();
        //noinspection ConstantConditions
        for (String name : channelFolder.list()) {
            name = name.substring(0, name.lastIndexOf('.'));
            Channel channel = load(name);
            addChannel(channel);
            channels.add(channel);
        }
        return channels;
    }
    
    @Override
    public void removeChannel(Channel channel) {
        configs.remove(channel);
        flagUpdate(channel);
    }
    
    @Override
    public void update() {
        Herochat.info("Saving channels");
        for (Channel channel : updates) {
            update(channel);
        }
        updates.clear();
        Herochat.info("Save complete");
    }
    
    @Override
    public void update(Channel c) {
        Channel channel = Herochat.getChannelManager().getChannel(c.getName());
        File file = new File(channelFolder, c.getName() + ".yml");
        if (channel != null) {
            FileConfiguration config = configs.get(channel);
            config.options().copyDefaults(true);
            config.set("name", channel.getName());
            config.set("nick", channel.getNick());
            config.set("format", channel.getFormat());
            if (channel.getPassword() != null) config.set("password", channel.getPassword());
            config.set("color", channel.getColor().name());
            config.set("distance", channel.getDistance());
            config.set("shortcut", channel.isShortcutAllowed());
            config.set("verbose", channel.isVerbose());
            config.set("crossworld", channel.isCrossWorld());
            config.set("cross-server", channel.isCrossServer());
            config.set("muted", channel.isMuted());
            config.set("worlds", new ArrayList<>(channel.getWorlds()));
            config.set("bans", new ArrayList<>(UUIDConverter.uuidToString(channel.getBansUUID())));
            config.set("mutes", new ArrayList<>(UUIDConverter.uuidToString(channel.getMutesUUID())));
            config.set("moderators", new ArrayList<>(UUIDConverter.uuidToString(channel.getModeratorsUUID())));
            config.set("spam.message-limit", c.getSpamMessageLimit());
            config.set("spam.message-limit-buildoff", c.getSpamMessageLimitBuildOff());
            try {
                config.save(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else //noinspection ResultOfMethodCallIgnored
            file.delete();
    }
    
}
