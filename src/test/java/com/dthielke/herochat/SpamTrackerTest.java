package com.dthielke.herochat;

import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

public class SpamTrackerTest {
    
    private SpamTracker tracker;
    private UUID chatter;
    
    @Before
    public void setUp() {
        tracker = new SpamTracker();
        tracker.setLimit(3);
        tracker.setBuildOff(100);
        
        chatter = UUID.randomUUID();
    }
    
    @Test
    public void testSpam() throws Exception {
        assertTrue(tracker.onChat(chatter));
        assertTrue(tracker.onChat(chatter));
        assertTrue(tracker.onChat(chatter));
        assertFalse(tracker.onChat(chatter));
        Thread.sleep(105);
        assertTrue(tracker.onChat(chatter));
    }

    
}
